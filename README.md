# Introduction to Qualitative and Unified Research Methods
This is the readMe file of the repository housing materials for the Introduction to Qualitative and Unified Research Methods PhD course.

## Basic Description of Course

### Background
Qualitative methods entail working with non-categorical and non-numeric data, which are usually collected via interviews, focus groups, observation, or open-ended survey questions. Unified methodology involves quantifying qualitative data in order to investigate patterns therein and to generate quantitative models for further exploration and supporting qualitative claims. Designing such research projects can be challenging, especially if one aims for transparency and verifiability throughout the process, from sampling to analyses and modelling. 


### Objectives and outcomes
The course aims to familiarize students with basic qualitative research design, including sampling, data collection methods, data management, coding and segmentation, analytical frameworks, as well as data modelling within a unified methodological paradigm.
By the end of the course, students should:
-	Be able to design a qualitative and/or unified project
-	Submit a preregistration for their study
-	Collect, code, and segment data
-	Perform various analyses on data
-	Be familiar with ways of visualizing and modelling quantified qualitative data

## Tools Employed During the Course

### Simple Qualitative Administration for File Organization and Development (SQAFFOLD)
Available at: https://www.sqaffold.org
Function: provide a basic directory structure, not only for the course, but also for future projects; help students practice proper project administration

### Reproducible Open Coding Kit (ROCK)
Available at: https://rock.science
Function: teach students a standard for working with qualitative data; provide an R package for processing qualitative data

### Interface for the Reproducible Open Coding Kit (iROCK)
Available at: https://i.rock.science
Function: enable the coding (and segmenting) of mock data in a way that complies with the ROCK standard and can be further processed with the R package

### Posit Cloud
Available at: https://posit.cloud
Function: provide an interface for working with ROCK functionality (supplements iROCK functionality until the comprehensive interface is developed)

### Epistemic Network Analysis
Available at:  https://app.epistemicnetwork.org/login.html
Function: provide an example of unified methodology; provide an opportunity to model qualitative data