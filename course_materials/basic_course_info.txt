Introduction to Qualitative and Unified Research Methods
(4207-A)

# Date: Oct. 6-10
Fri 3pm-6pm (4x45 min; 3 hrs)
Sat 10am-4pm (8x45 min; 6 hrs)
Sun 10am-4pm (8x45 min; 6 hrs)
Mon 10am-1pm (4x45 min; 3 hrs)
Tues 10am-1pm (4x45 min; 3 hrs)
----Total: 28x45 min; 21 hrs

# Place: Library of the Institute of Behavioral Sciences, Semmelweis University
https://semmelweis.hu/magtud/en/contact

# Contact: Szilvia Zörgő
06 30 86 22 466
zorgoszilvia@gmail.com

# Overview
## Module 1 (Fri, 4x45min)
### Overview and requirements
### Paradigms and research questions (RQs)
### Administration and preregistration

## Module 2 (Sat, 8x45min)
### Sampling
### Data collection and instruments
### Natural language processing (NLP)
### Code development

## Module 3 (Sun, 8x45min)
### Analytical frameworks
### Computer-assisted qualitative data analysis software (CAQDAS)
### Inter-rater reliability / Coder agreement (IRR)

## Module 4 (Mon, 4x45min)
### Quantification
### Frequencies, visualizations

## Module 5 (Tue, 4x45min)
### Modelling
### Reporting
