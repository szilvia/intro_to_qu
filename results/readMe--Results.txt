For guidance on reporting standards in qualitative research, see:

Consolidated criteria for reporting qualitative research (COREQ): a 32-item checklist for interviews and focus groups (Tong et al.)

Available at: 
https://www.equator-network.org/reporting-guidelines/coreq