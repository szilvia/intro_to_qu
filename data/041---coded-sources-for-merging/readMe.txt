During the course, we are only working with merged sources on the posit Cloud project, not locally.
Please see the corresponding directory at https://posit.cloud/content/6589471 for mock coded data.