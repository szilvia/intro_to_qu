[[cid=3]]

My anger, my anger scares me. [Interviewer] Can you give us an example of a time where your anger scared you? [Interviewee] Yeah, like I found out my boyfriend was cheating on me and I was afraid I might kill the girl. [Interviewer] You were what? [Interviewee] I was afraid I might kill her. Not like murder. That scared me, you know? 