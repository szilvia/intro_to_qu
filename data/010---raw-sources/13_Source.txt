[[cid=13]]

As much as we want to believe that we know ourselves more than anybody else, there is so much unpredictability; infinitely, actually. We are not the same person two seconds ago that we are two seconds later. Without our permission. And I think that's what scares me.